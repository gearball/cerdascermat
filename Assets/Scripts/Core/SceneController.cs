﻿using UnityEngine;
using System.Collections;
using CerdasCermat.GUI;
using CerdasCermat.Utils;
using SmartLocalization;
using NullReferenceException = System.NullReferenceException;

namespace CerdasCermat.Core {

	public abstract class SceneController : MonoBehaviour {

		// Use this for initialization
		void Start () {
			OnStart ();
			InitLayout ();
			StartCoroutine (LateInit());
		}
		
		// Update is called once per frame
		void Update () {
			OnUpdate ();
		}

		IEnumerator LateInit () {
			yield return null;
			OnLateInit ();
		}

		protected virtual void OnStart () {}

		protected virtual void OnUpdate () {}

		protected virtual void OnLateInit () {}

		protected virtual void InitLayout () {}
		
		public void LoadLevel (Scenes scene) {
			Application.LoadLevel (scene.ToString());
		}
	}

}
