namespace CerdasCermat.Core {

	public enum Scenes {
		Splash,
		Login,
		Home,
		Gameplay
	}
	
	public enum Results {
		Success,
		Failed
	}
	
	public enum Commands {
		SignIn,
		Register,
		GetSchools,
		AddSchool,
		GetVersion,
		GetCategory,
		GetQuestion
	}
	
	public enum Tags {
		Result,
		Message,
		Success,
		Failed,
		Score,
		Email,
		Command,
		Value,
		Username,
		Password,
		School,
		City,
		Version,
		Category,
		Count,
		Question,
		Choice,
		Correct,
		Incorrect,
		Multiplier
	}

	public enum Localizations {
		Marks,
		Point
	}

}
