using UnityEngine;
using System.Collections;
using CerdasCermat.Utils;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json.Linq;

namespace CerdasCermat.Core {

	/// <summary>
	/// Contains persistent objects throughout the app.
	/// </summary>
	public class Singleton : MonoBehaviour {

		#region Core

		/// <summary>
		/// Protected initializator to prevent multiple reference.
		/// </summary>
		protected Singleton () {}

		/// <summary>
		/// Gets a value indicating whether this <see cref="Reminders.Core.Singleton"/> is instantiated.
		/// </summary>
		/// <value><c>true</c> if is instantiated; otherwise, <c>false</c>.</value>
		public static bool isInstantiated {
			get;
			private set;
		}

		/// <summary>
		/// Prevents error when the app is closed.
		/// </summary>
		static bool isFinished = false;

		/// <summary>
		/// Instance reference for this Singleton.
		/// </summary>
		static Singleton instance = null;

		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <value>The instance.</value>
		static Singleton Instance {
			get {
				if (isFinished)
					return null;
				if (!isInstantiated) {
					GameObject singleton = new GameObject();
					singleton.name = "Singleton";
					instance = singleton.AddComponent<Singleton>();
					isInstantiated = true;

					DontDestroyOnLoad(singleton);

					Initialize ();
				}
				return instance;
			}
		}

		/// <summary>
		/// Raises the destroy event.
		/// </summary>
		void OnDestroy () {
			isFinished = true;
		}

		/// <summary>
		/// Initialize this instance.
		/// </summary>
		public static void Initialize () {
			PlayFabSettings.TitleId = Configurations.TitleId;
		}

		#endregion

		#region Data Persistent
		
		/// <summary>
		/// User data on PlayFab.
		/// </summary>
		JObject userData;

		/// <summary>
		/// Gets or sets the user data.
		/// </summary>
		/// <value>The user data.</value>
		public static string UserData {
			get {
				return Instance.userData.ToString();
			}
			set {
				Instance.userData = JObject.Parse(value);
			}
		}
		
		/// <summary>
		/// Category list from own server.
		/// </summary>
		JObject category;
		
		/// <summary>
		/// Gets or sets the category list.
		/// </summary>
		/// <value>Category.</value>
		public static string Category {
			get {
				if (Instance.category != default(JObject)) {
					return Instance.category.ToString();
				} else {
					string tmp = PlayerPrefs.GetString (Tags.Category.ToString());
					if (tmp != "") {
						Instance.category = JObject.Parse(tmp);
					}
					return Instance.category.ToString();
				}
			}
			set {
				PlayerPrefs.SetString (Tags.Category.ToString(), value);
				Instance.category = JObject.Parse(value);
			}
		}
		/// <summary>
		/// Gets or sets the category list as JObject.
		/// </summary>
		/// <value>category.</value>
		public static JObject CategoryData {
			get {
				if (Instance.category != default(JObject)) {
					return Instance.category;
				} else {
					string tmp = PlayerPrefs.GetString (Tags.Category.ToString());
					if (tmp != "") {
						Instance.category = JObject.Parse(tmp);
					}
					return Instance.category;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the content version.
		/// </summary>
		/// <value>Version.</value>
		public static int Version {
			get {
				return PlayerPrefs.GetInt (Tags.Version.ToString());
			}
			set {
				PlayerPrefs.SetInt (Tags.Version.ToString(), value);
			}
		}

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>username.</value>
		public static string Username {
			get {
				return PlayerPrefs.GetString (Tags.Username.ToString());
			}
			set {
				PlayerPrefs.SetString (Tags.Username.ToString(), value);
			}
		}
		
		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>password.</value>
		public static string Password {
			get {
				return PlayerPrefs.GetString (Tags.Password.ToString());
			}
			set {
				PlayerPrefs.SetString (Tags.Password.ToString(), value);
			}
		}

		public static string CurrentCategory;

		public static JArray CurrentQuestions; 

		#endregion

		#region Public Methods

		public static JObject PlaceList;
		public static string City;
		public static string School;
		public static string Email;
		public static long Score;

		public static int Multiplier {
			get {
				return int.Parse((string) (Instance.userData[Tags.Multiplier.ToString()][Tags.Value.ToString()]));
			}
		}

		#endregion
	}

}

