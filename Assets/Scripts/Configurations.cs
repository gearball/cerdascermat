﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CerdasCermat.Core;

public class Configurations : MonoBehaviour {
	
	public const string TitleId = "E41B";

	public const string ExternalURL = "http://kuverastudio.com/cerdascermat/control.php";

	public const string InternalDataPath = "Data/";
	
	public const string CharList = "ABCDEF0123456789";
	
	public const int PasswordBasicLength = 6;

	public const string SchoolPassword = "SchoolPassword";

	public const int TotalQuestion = 10;

	public const int InitialMultiplier = 1;

	public const float BaseTimeLimit = 30f;

	public const int BasePoint = 1000;
}
