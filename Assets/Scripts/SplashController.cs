﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CerdasCermat.Core;
using CerdasCermat.GUI;
using CerdasCermat.Communication;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json.Linq;

public class SplashController : SceneController {
	
	[SerializeField] AnimatedLayout layoutTitle;

	// Use this for initialization
	protected override void OnStart () {
		layoutTitle.Activate (true, true);
	}
	
	// Update is called once per frame
	protected override void OnUpdate () {

	}

	public void AutoSignIn () {
		if (Singleton.Username != "") {
			JObject data = new JObject ();
			data.Add (Tags.Command, Commands.SignIn);
			data.Add (Tags.Username, Singleton.Username);
			data.Add (Tags.Password, Singleton.Password);
			Serverlink.LoadExternalData (
					Configurations.ExternalURL,
					(result) => {
					if ((string)result[Tags.Result] == Tags.Success.ToString()) {
						JObject detail = (JObject)result[Tags.Message];
						Singleton.City = (string)detail[Tags.City];
						Singleton.School = (string)detail[Tags.School];
						Singleton.Email = (string)detail[Tags.Email];
						Singleton.Score = long.Parse((string)detail[Tags.Score]);
						LoadLevel (Scenes.Home);
					} else {
						Debug.Log((string)result[Tags.Result]);
					}
				},
				(error) => {
					Debug.Log(error);
				},
				data);
		}
	}

	void HandleError () {
		Debug.Log ("Error, Need Restart");
	}
}