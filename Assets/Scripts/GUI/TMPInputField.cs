﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

namespace CerdasCermat.GUI {

	[RequireComponent(typeof(InputField))]
	public class TMPInputField : MonoBehaviour {

		[SerializeField] TextMeshProUGUI TMPPlaceholder;
		[SerializeField] TextMeshProUGUI TMPText;
		
		InputField inputField;
		GameObject inputCaret;

		void Start () {
			inputField = GetComponent<InputField> ();
			inputField.onValueChange.AddListener(UpdateText);
		}

		void UpdateText (string newText) {
			string output = "";
			if (inputField.contentType == InputField.ContentType.Password)
				for (int i = 0; i < newText.Length; i++) {
					output += "*";
				}
			else
				output = newText;
			TMPText.text = output;
			TMPPlaceholder.gameObject.SetActive (output.Length == 0);

		}
		
		public void DisableCaret () {
			if (inputCaret == null && inputField.transform.Find (inputField.name + " Input Caret") != null)
				inputCaret = inputField.transform.Find (inputField.name + " Input Caret").gameObject;
			if (inputCaret != null)
				inputCaret.SetActive (false);
		}
	}

}