﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace CerdasCermat.GUI {
	
	[CustomEditor(typeof(AnimatedLayout))]
	public class AnimatedLayoutEditor : Editor {

		void OnEnable () {
			
		}

		public override void OnInspectorGUI () {

			EditorGUILayout.PropertyField (serializedObject.FindProperty("fadeTime"));
			EditorGUILayout.PropertyField (serializedObject.FindProperty("scaleFactor"));
			EditorGUILayout.PropertyField (serializedObject.FindProperty("scaleTime"));

			SerializedProperty useOnActivated = serializedObject.FindProperty ("useOnActivated");
			useOnActivated.boolValue = EditorGUILayout.Toggle ("On Activated", useOnActivated.boolValue);
			if (useOnActivated.boolValue) {
				EditorGUILayout.PropertyField (serializedObject.FindProperty("OnActivated"));
			}

			SerializedProperty useOnDeactivated = serializedObject.FindProperty ("useOnDeactivated");
			useOnDeactivated.boolValue = EditorGUILayout.Toggle ("On Deactivated", useOnDeactivated.boolValue);
			if (useOnDeactivated.boolValue) {
				EditorGUILayout.PropertyField (serializedObject.FindProperty("OnDeactivated"));
			}

			serializedObject.ApplyModifiedProperties ();
		}
	}

}
