﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using TMPro;
using UnityEngine.UI;
using CerdasCermat.Utils;

namespace CerdasCermat.GUI {

	public class AutoCompleteChoices : MonoBehaviour {

		[SerializeField] TextMeshProUGUI[] texts;

		public int Available {
			get;
			private set;
		}

		public class OnSelected : UnityEvent<string> {};

		public OnSelected onSelected;

		List<string> choices;
		public List<string> Choices {
			get {
				return choices;
			}
			set {
				choices = value;
			}
		}

		void Awake () {
			onSelected = new OnSelected ();
		}

		// Use this for initialization
		void Start () {
			gameObject.SetActive (false);
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void Hide () {
			for (int i = 0; i < texts.Length; i++) {
				texts[i].transform.parent.gameObject.SetActive (false);
			}
			gameObject.SetActive(false);
			Available = 0;
		}

		public void PopulateChoices (string reference) {
			Hide ();
			if (reference.Length == 0) {
				return;
			}
			string b = Util.SuperTrim(reference);
			for (int i = 0; i < choices.Count; i++) {
				string current = choices[i];
				string a = Util.SuperTrim(current);
				bool similar = a.Length >= b.Length && a.Substring(0,b.Length) == b;
				if (Available < texts.Length && similar) {
					gameObject.SetActive(true);
					texts[Available].text = current;
					GameObject choice = texts[Available].transform.parent.gameObject;
					choice.gameObject.SetActive (true);
					choice.GetComponent<Button> ().onClick.AddListener (
						() => {
						onSelected.Invoke (current);
						Hide ();
					});
					Available++;
					if (Available == texts.Length)
						break;
				}
			}
		}

		public string GetChoiceString (int index) {
			return texts [index].text;
		}


	}

}
