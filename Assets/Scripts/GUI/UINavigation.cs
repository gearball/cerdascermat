﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace CerdasCermat.GUI {
	
	public class UINavigation : MonoBehaviour {

		public GameObject previousSelectable;
		public GameObject nextSelectable;

		private EventSystem system;
		int counter;
		
		void Start ()
		{
			system = EventSystem.current;
		}
		
		public void Update()
		{	
			if (counter > 10 && Input.GetKeyUp (KeyCode.Tab) && system.currentSelectedGameObject == gameObject)
			{
				Selectable next = null;
				
				if(Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))
				{
					if (previousSelectable != null)
						next = previousSelectable.GetComponent<Selectable>();
				}
				else 
				{
					if (nextSelectable != null)
						next = nextSelectable.GetComponent<Selectable>();
				}

				if (next!= null) 
				{
					system.SetSelectedGameObject(next.gameObject);

					InputField inputfield = next.GetComponent<InputField> ();
					if (inputfield != null) inputfield.OnPointerClick (new PointerEventData (system));

					next.GetComponent<UINavigation>().counter = 0;
				}
			}
			counter++;
		}
	}
}
