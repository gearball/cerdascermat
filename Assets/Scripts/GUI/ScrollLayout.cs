﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace CerdasCermat.GUI {

	public class ScrollLayout : MonoBehaviour {

		[SerializeField] float animTime;
		[SerializeField] RectTransform content;

		RectTransform child;
		LayoutElement layout;
		Vector2 originPreferredSize;

		void Awake () {
			layout = GetComponent<LayoutElement> ();
			originPreferredSize = new Vector2 (content.rect.width, content.rect.height);
		}

		void Start () {

		}

		void Update () {
			if (originPreferredSize == default(Vector2) && !content.sizeDelta.Equals(Vector2.zero)) {
				originPreferredSize = content.sizeDelta;
			}
		}

		public void RecheckSize (bool show) {
			StartCoroutine (recheckSize (show));
		}

		IEnumerator recheckSize (bool show) {
			yield return null;
			originPreferredSize = content.sizeDelta;
			layout.preferredWidth = content.sizeDelta.x;
			layout.preferredHeight = content.sizeDelta.y;
			Activate (show);
		}

		public void Activate (bool show) {
			layout.DOPreferredSize (
				new Vector2 (originPreferredSize.x, show ? originPreferredSize.y : 0f),
				animTime
				);
		}
	}

}
