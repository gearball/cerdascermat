﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using DG.Tweening;

namespace CerdasCermat.GUI {

	[RequireComponent(typeof(CanvasGroup))]
	public class AnimatedLayout : MonoBehaviour {

		[SerializeField] float fadeTime = 1;
		[SerializeField] float scaleFactor = 0;
		[SerializeField] float scaleTime = 0.5f;
		public UnityEvent OnActivated;
		public UnityEvent OnDeactivated;

		[SerializeField] bool useOnActivated;
		[SerializeField] bool useOnDeactivated;

		CanvasGroup canvasGroup;
		Tweener activeTween;

		void Awake () {
			canvasGroup = GetComponent<CanvasGroup> ();
			Activate (false);
		}

		public bool IsActive {
			get;
			private set;
		}

		public bool IsAnimating {
			get;
			private set;
		}

		public void Activate (bool value) {
			Activate (value, false);
		}
			
		public void Activate (bool value, bool fading) {
			if (fading) {
				if (scaleFactor != 0 && activeTween == null)
					transform.DOScale ((Vector3.right + Vector3.up) * scaleFactor + Vector3.forward, scaleTime).SetLoops(2, LoopType.Yoyo).SetId(gameObject.name);
				if (activeTween != null) {
					activeTween.Kill (true);
					DOTween.Kill(gameObject.name,true);
				}
				activeTween = canvasGroup.DOFade (value ? 1f : 0f, fadeTime).OnComplete (
					() => {
						if (value)
							OnActivated.Invoke ();
						else
							OnDeactivated.Invoke ();
						Interactable (value);
						IsActive = value;
						DOTween.Kill(gameObject.name,true);
						activeTween = null;
						IsAnimating = false;
					});
				IsAnimating = true;
			} else {
				Interactable (value);
				IsActive = value;
				IsAnimating = false;
			}
		}

		void Interactable (bool value) {
			canvasGroup.interactable = value;
			canvasGroup.blocksRaycasts = value;
			canvasGroup.alpha = value ? 1f : 0f;
		}

		public void Blink () {
			if (activeTween != null) {
				activeTween.Kill (true);
				DOTween.Kill(gameObject.name,true);
			}
			Interactable (true);
			canvasGroup.DOFade (0.2f, fadeTime).SetLoops (-1, LoopType.Yoyo);
		}

		public bool IsInteractable {
			get {
				return canvasGroup.interactable;
			}
			set {
				canvasGroup.interactable = value;
			}
		}

	}

}
