﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

namespace CerdasCermat.GUI {

	public class CategoryContainer : MonoBehaviour {

		#region Hooked UI Components

		[SerializeField] TextMeshProUGUI title;
		[SerializeField] Image[] icons;
		[SerializeField] Transform container;
		[SerializeField] Toggle toggle;

		#endregion

		#region Core


		#endregion

		public void SetTitle (string input) {
			title.text = input;
		}

		public void SetImage (Sprite sprite) {
			icons [0].sprite = sprite;
			icons [1].sprite = sprite;
		}

		public void Activate (bool show) {
			toggle.isOn = false;
		}

		public void SetToggleGroup (ToggleGroup group) {
			toggle.group = group;
		}
	}

}