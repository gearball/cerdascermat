﻿using UnityEngine;
using System.Collections;
using TMPro;

namespace CerdasCermat.GUI {

	public class TextLayout : MonoBehaviour {

		[SerializeField] Vector4 newMargins;
		bool isMarginActive;
		Vector4 originalMargins;

		void Awake () {
			originalMargins = GetComponent<TextMeshProUGUI>().margin;
		}
		
		public void UseMargin (bool input) {
			if (transform.parent.GetComponent<UnityEngine.UI.Button> ().interactable) {
				isMarginActive = input;
				GetComponent<TextMeshProUGUI> ().margin = isMarginActive ? newMargins : originalMargins;
			}
		}
	}

}