using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using CerdasCermat.Utils;
using CerdasCermat.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace CerdasCermat.Communication {

	public class Serverlink : MonoBehaviour {

		static Serverlink instance;

		static Serverlink Instance {
			get {
				if( !instance )
				{
					// check if there is a GO instance already available in the scene graph
					instance = FindObjectOfType (typeof(Serverlink)) as Serverlink;
					
					// nope, create a new one
					if( !instance )
					{
						var obj = new GameObject ("Serverlink");
						instance = obj.AddComponent<Serverlink> ();
						DontDestroyOnLoad (obj);
					}
				}
				
				return instance;
			}
		}

		public static void LoadExternalData (string url, UnityAction<JObject> onLoaded, UnityAction<JObject> onError, JObject data = null) {
			Debug.Log ("Attempting to Retrieve External Data...");
			Instance.StartCoroutine (Instance.loadExternalData (url, onLoaded, onError, data));
		}

		IEnumerator loadExternalData(string url, UnityAction<JObject> onLoaded, UnityAction<JObject> onError, JObject data = null)
		{
			WWWForm form = new WWWForm ();
			if (data != null) {
				foreach (var entry in data) {
					form.AddField (entry.Key.ToString(), entry.Value.ToString());
				}
			}

			var headers = form.headers;
			
			if (!headers.ContainsKey("Content-Type"))
			{
				Debug.Log("Header Updated!");
				headers.Add("Content-Type", "application/x-www-form-urlencoded");
			}

			WWW w = data != null ? new WWW (url, form) : new WWW (url);
			yield return w;
			if (w.error != null)
			{
				Debug.Log("Error loading External Data");
				JObject error = new JObject ();
				error.Add ("Message", w.error);
				onError.Invoke (error);
			}
			else
			{
				Debug.Log ("External Data loaded!");
				onLoaded.Invoke (JObject.Parse(w.text));
			}
		}

		
	}
	
}
