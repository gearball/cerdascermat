﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CerdasCermat.Core;
using CerdasCermat.GUI;
using CerdasCermat.Communication;
using TMPro;
using Newtonsoft.Json.Linq;

public class HomeController : SceneController {
	
	#region Hooked UI Components

	[SerializeField] Button btnPlay;
	
	[Header("Layouts")]
	[SerializeField] ScrollLayout schoolLayout;
	[SerializeField] ScrollLayout userLayout;
	[SerializeField] ScrollLayout settingsLayout;
	[SerializeField] ScrollLayout informationLayout;

	[Header("School")]
	[SerializeField] TextMeshProUGUI txtSchoolName;

	[Header("User")]

	[Header("Settings")]
	[SerializeField] TextMeshProUGUI txtUserName;
	[SerializeField] TextMeshProUGUI txtPassword;
	[SerializeField] Button btnLogout;

	#endregion

	#region Core

	// Use this for initialization
	protected override void OnStart () {

		txtSchoolName.text = Singleton.School;
		txtUserName.text = Singleton.Username;
		txtPassword.text = HideString (Singleton.Password);
	}
	
	// Update is called once per frame
	protected override void OnUpdate () {

	}

	protected override void OnLateInit () {
		schoolLayout.Activate (true);
		userLayout.Activate (false);
		settingsLayout.Activate (false);
		informationLayout.Activate (false);
	}

	#endregion
	
	public void ShowPassword (bool show) {
		if (show) {
			txtPassword.text = Singleton.Password;
		} else {
			txtPassword.text = HideString (txtPassword.text);
		}
	}
	
	string HideString (string input) {
		string output = "";
		for (int i = 0; i < input.Length; i++) {
			output += "*";
		}
		return output;
	}

	public void Logout () {
		PlayerPrefs.DeleteKey (Tags.Username.ToString ());
		PlayerPrefs.DeleteKey (Tags.Password.ToString ());
		LoadLevel (Scenes.Login);
	}

	public void Play () {
		btnPlay.interactable = false;
		string time = "09-12";
		JArray array = (JArray)(Singleton.CategoryData [time]);
		string category = (string)(array [Random.Range (0, array.Count)]);
		JObject data = new JObject ();
		data.Add (Tags.Command.ToString (), Commands.GetQuestion.ToString ());
		data.Add (Tags.Category.ToString (), "Kartun");//category);
		data.Add (Tags.Count.ToString (), Configurations.TotalQuestion);
		Singleton.CurrentCategory = category;
		Serverlink.LoadExternalData (
			Configurations.ExternalURL,
			(result) => {
				Debug.Log (result);
				Singleton.CurrentQuestions = (JArray)result[Tags.Message];
				btnPlay.interactable = true;
				LoadLevel (Scenes.Gameplay);
			}, 
			(error) => {
				btnPlay.interactable = true;
			},
			data);
	}
}