﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using TMPro;
using SmartLocalization;
using CerdasCermat.Core;
using CerdasCermat.GUI;
using CerdasCermat.Communication;
using CerdasCermat.Utils;
using DG.Tweening;

public class LoginController : SceneController {

	#region Hooked UI Components

	[Header("Layouts")]
	[SerializeField] RectTransform wholeLayout;
	[SerializeField] ScrollLayout signinLayout;
	[SerializeField] ScrollLayout registerLayout;
	[SerializeField] AnimatedLayout setAction;
	[SerializeField] AnimatedLayout imgLoading;
	[SerializeField] Button btnSignInProcess;
	[SerializeField] Button btnRegisterProcess;
	[SerializeField] InputField inpSignInUsername;
	[SerializeField] InputField inpSignInPassword;
	[SerializeField] InputField inpRegisterUsername;
	[SerializeField] InputField inpRegisterCity;
	[SerializeField] AutoCompleteChoices inpRegisterCityChoices;
	[SerializeField] Toggle tglRegisterCityWarning;
	[SerializeField] InputField inpRegisterSchool;
	[SerializeField] AutoCompleteChoices inpRegisterSchoolChoices;
	[SerializeField] Toggle tglRegisterSchoolWarning;
	[SerializeField] TextMeshProUGUI txtLog;

	#endregion

	#region Internal Variables

	string tmpPassword;

	bool isSignIn;
	bool isProcessing;

	string currentCity;
	string currentSchool;

	bool isShiftedUp;

	#endregion

	#region Core
	
	// Use this for initialization
	protected override void OnStart () {

		registerLayout.Activate (false);

		setAction.Activate (true);
		setAction.IsInteractable = false;

		isShiftedUp = false;

		isSignIn = true;
		Singleton.Initialize ();

		tglRegisterCityWarning.image.enabled = false;
		tglRegisterSchoolWarning.image.enabled = false;

		ShowLoading (true);
		JObject data = new JObject ();
		data.Add (Tags.Command, Commands.GetSchools);
		Serverlink.LoadExternalData (
			Configurations.ExternalURL,
			(result) => {
				setAction.IsInteractable = true;
				Singleton.PlaceList = (JObject)result[Tags.Message];
				inpRegisterCityChoices.Choices = Singleton.PlaceList.Properties().Select(p=>p.Name).ToList<string>();
				ShowLoading(false);
			},
			(error) => {
				txtLog.text = (string)error["Message"];
				ShowLoading (false);
			},
			data);

		inpRegisterCityChoices.onSelected.AddListener(
			(text) => {
			inpRegisterCity.text = text;
			tglRegisterCityWarning.image.enabled = false;
			tglRegisterCityWarning.isOn = false;
		});

		inpRegisterCity.onValueChange.AddListener (
			(city) => {
				inpRegisterCityChoices.PopulateChoices (city);
				bool exist = false;
				for (int i = 0; i < inpRegisterCityChoices.Available; i++) {
					string id = inpRegisterCityChoices.GetChoiceString(i);
					if (Util.SuperTrim(id) == Util.SuperTrim(city)) {
						exist = true;
						currentCity = id;
						if (Singleton.PlaceList[id].Count() > 0) {
							inpRegisterSchoolChoices.Choices = Singleton.PlaceList[id].ToObject<List<string>>();
							inpRegisterSchoolChoices.onSelected.AddListener(
								(school) => {
									inpRegisterSchool.text = school;
									tglRegisterSchoolWarning.image.enabled = false;
									tglRegisterSchoolWarning.isOn = false;
								});
						} else {
							inpRegisterSchoolChoices.Choices = new List<string>();
						}
						ShowLoading(false);
						break;
					}
				}
				if (!exist)
					currentCity = "";
				tglRegisterCityWarning.image.enabled = !exist && city != "";
				if (!tglRegisterCityWarning.image.enabled)
					tglRegisterCityWarning.isOn = false;
				registerLayout.RecheckSize(true);
			});

		inpRegisterCity.onEndEdit.AddListener (
			(city) => {
				if (currentCity.Length > 0) {
					inpRegisterCity.text = currentCity;
				}
			});
		
		inpRegisterSchool.onValueChange.AddListener (
			(school) => {
				inpRegisterSchoolChoices.PopulateChoices (school);
				bool exist = false;
				for (int i = 0; i < inpRegisterSchoolChoices.Available; i++) {
					string id = inpRegisterSchoolChoices.GetChoiceString(i);
					if (Util.SuperTrim(id) == Util.SuperTrim(school)) {
						exist = true;
						currentSchool = id;
						break;
					}
				}
				if (!exist)
					currentSchool = "";
				tglRegisterSchoolWarning.image.enabled = !exist && school != "";
				if (!tglRegisterSchoolWarning.image.enabled)
					tglRegisterSchoolWarning.isOn = false;
				registerLayout.RecheckSize(true);
			});

		inpRegisterSchool.onEndEdit.AddListener (
			(school) => {
				if (currentSchool.Length > 0) {
					inpRegisterSchool.text = currentSchool;
				}
			});
	}

	// Update is called once per frame
	protected override void OnUpdate () {
		if (!isProcessing) {
			if (isSignIn) {
				btnSignInProcess.interactable = inpSignInUsername.text != "" && inpSignInPassword.text != "";
			} else {			
				btnRegisterProcess.interactable = inpRegisterUsername.text != "" && inpRegisterSchool.text != "" && inpRegisterCity.text != "" && !tglRegisterCityWarning.image.enabled;
				inpRegisterSchool.interactable = inpRegisterCity.text != "" && !tglRegisterCityWarning.image.enabled;
			}
		}
	}

	#endregion

	/// <summary>
	/// Process Register / Sign In.
	/// </summary>
	public void Process () {
		if (isSignIn) {
			ProcessSignIn ();
		} else {
			ProcessRegister ();
		}
		ShowLoading (true);
	}

	/// <summary>
	/// Switches layout to Sign In.
	/// </summary>
	public void SignInLayout (bool active) {
		if (!isSignIn && active)
			isSignIn = true;
	}
	
	/// <summary>
	/// Switches layout to Register.
	/// </summary>
	public void RegisterLayout (bool active) {
		if (isSignIn && active)
			isSignIn = false;
	}

	/// <summary>
	/// Show loading image and animation.
	/// </summary>
	/// <param name="show">If set to <c>true</c> show.</param>
	public void ShowLoading (bool show) {
		isProcessing = show;
		imgLoading.Activate (show, true);
		setAction.IsInteractable = !show;
		if (show)
			txtLog.text = "";
	}

	/// <summary>
	/// Send Sign In data to server.
	/// </summary>
	void ProcessSignIn ()
	{
		JObject data = new JObject ();
		data.Add (Tags.Command, Commands.SignIn);
		data.Add (Tags.Username, inpSignInUsername.text);
		data.Add (Tags.Password, inpSignInPassword.text);
		Serverlink.LoadExternalData (
			Configurations.ExternalURL,
			(result) => {
				if ((string)result[Tags.Result] == Tags.Success.ToString()) {
					JObject detail = (JObject)result[Tags.Message];
					Singleton.Username = inpSignInUsername.text;
					Singleton.Password = inpSignInPassword.text;
					Singleton.City = (string)detail[Tags.City];
					Singleton.School = (string)detail[Tags.School];
					Singleton.Email = (string)detail[Tags.Email];
					Singleton.Score = long.Parse((string)detail[Tags.Score]);
					LoadLevel (Scenes.Home);
				} else {
					txtLog.text = (string)result[Tags.Result];
					ShowLoading (false);
				}
			},
			(error) => {
				txtLog.text = (string)error["Message"];
				ShowLoading (false);
			},
			data);
	}
	
	/// <summary>
	/// Send Register data to server.
	/// </summary>
	void ProcessRegister () {

		if (currentSchool.Length == 0) {
			AddNewSchool ();
		} else {
			AddNewUser ();
		}
	}

	void AddNewSchool () {
		JObject data = new JObject ();
		data.Add (Tags.Command, Commands.AddSchool);
		data.Add (Tags.School, inpRegisterSchool.text);
		data.Add (Tags.City, inpRegisterCity.text);
		Serverlink.LoadExternalData (
			Configurations.ExternalURL,
			(result) => {
				if ((string)result[Tags.Result] == Tags.Success.ToString()) {
					AddNewUser ();
				} else {
					txtLog.text = (string)result[Tags.Result];
					ShowLoading (false);
				}
			},
			(error) => {
				txtLog.text = (string)error["Message"];
				ShowLoading (false);
			},
			data);
	}

	void AddNewUser () {
		tmpPassword = RandomString (Configurations.PasswordBasicLength, Configurations.CharList);
		JObject data = new JObject ();
		data.Add (Tags.Command, Commands.Register);
		data.Add (Tags.Username, inpRegisterUsername.text);
		data.Add (Tags.Password, tmpPassword);
		data.Add (Tags.Score, 0);
		data.Add (Tags.Email, "");
		data.Add (Tags.City, inpRegisterCity.text);
		data.Add (Tags.School, inpRegisterSchool.text);
		Serverlink.LoadExternalData (
			Configurations.ExternalURL,
			(result) => {
				if ((string)result[Tags.Result] == Tags.Success.ToString()) {
					Singleton.Password = (string)data[Tags.Password];
					Singleton.Username = (string)data[Tags.Username];
					Singleton.City = (string)data[Tags.City];
					Singleton.School = (string)data[Tags.School];
					Singleton.Email = (string)data[Tags.Email];
					Singleton.Score = 0;
					LoadLevel (Scenes.Home);
				} else {
					txtLog.text = (string)result[Tags.Result];
					ShowLoading (false);
				}
			},
			(error) => {
				txtLog.text = (string)error["Message"];
				ShowLoading (false);
			},
			data);
	}

	/// <summary>
	/// Get random string from listed characters.
	/// </summary>
	/// <returns>The string.</returns>
	/// <param name="length">Length.</param>
	string RandomString(int length, string chars)
	{
		string output = "";
		for (int i = 0; i < length; i++) {
			output += chars[Random.Range(0,chars.Length)].ToString();
		}
		return output;
	}

	/// <summary>
	/// Moves the whole layout up / down if there's an input.
	/// </summary>
	/// <param name="up">If set to <c>true</c> up.</param>
	public void MoveWholeLayout (bool up) {
		if (!isShiftedUp && up) 
			isShiftedUp = true;
		else if (isShiftedUp && !up)
			isShiftedUp = false;
		else
			return;
		Vector2 direction = Vector2.up * (up ? 230 : -270);
		wholeLayout.DOAnchorPos (direction, 0.5f);
	}
}