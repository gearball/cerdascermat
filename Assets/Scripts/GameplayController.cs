﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using CerdasCermat.Core;
using CerdasCermat.GUI;
using TMPro;
using Newtonsoft.Json.Linq;
using SmartLocalization;

public class GameplayController : SceneController {
	
	#region Hooked UI Components

	[SerializeField] TextMeshProUGUI txtCategory;
	[SerializeField] TextMeshProUGUI txtQuestion;
	[SerializeField] TextMeshProUGUI[] txtChoices;
	[SerializeField] Image timer;
	[SerializeField] TextMeshProUGUI txtPoint;
	[SerializeField] TextMeshProUGUI txtMarks;

	#endregion

	#region Internal Variables 

	bool isPlaying;
	float timeLeft;
	int currentIndex;
	string keyMarks;
	string keyPoint;
	int correct;
	int incorrect;
	int multiplier;
	int point;

	#endregion
	
	#region Core
	
	// Use this for initialization
	protected override void OnStart () {
		txtCategory.text = Singleton.CurrentCategory;

		currentIndex = -1;
		correct = 0;
		incorrect = 0;
		point = 0;
		multiplier = Singleton.Multiplier;

		keyPoint = LanguageManager.Instance.GetTextValue (Localizations.Point.ToString ());
		txtPoint.text = string.Format(keyPoint, point);

		keyMarks = LanguageManager.Instance.GetTextValue (Localizations.Marks.ToString ());
		txtMarks.text = string.Format(keyMarks, correct, incorrect);

		LoadNextQuestion ();

		GameStart ();
	}
	
	// Update is called once per frame
	protected override void OnUpdate () {
		
	}
	
	protected override void OnLateInit () {

	}
	
	#endregion

	void GameStart () {
		timeLeft = Configurations.BaseTimeLimit;
		isPlaying = true;
		StartCoroutine (UpdateTimer());
	}

	IEnumerator UpdateTimer () {
		while (isPlaying) {
			yield return null;
			timeLeft -= Time.deltaTime;
			timer.fillAmount = timeLeft / Configurations.BaseTimeLimit;
			if (timeLeft <= 0) {
				isPlaying = false;
			}
		}
	}

	public void LoadNextQuestion () {
		currentIndex++;
		if (currentIndex == Singleton.CurrentQuestions.Count) {
			LoadLevel (Scenes.Home);
			return;
			// kalo pass, currentIndex = 0;

		}
		txtQuestion.text = (string)(Singleton.CurrentQuestions[currentIndex][Tags.Question.ToString()]);
		int correctIdx = (int)(Singleton.CurrentQuestions[currentIndex][Tags.Correct.ToString()]);

		for (int i = 0; i < txtChoices.Length; i++) {
			JArray choiceList = (JArray)(Singleton.CurrentQuestions[currentIndex][Tags.Choice.ToString()]);
			txtChoices[i].text = (string)choiceList[i];
			Button btnChoice = GetButton (txtChoices [i].transform);
			btnChoice.onClick.RemoveAllListeners ();
			
			if (i == correctIdx) {
				btnChoice.onClick.AddListener(
					() => {
						correct++;
						txtMarks.text = string.Format(keyMarks, correct, incorrect);
						LoadNextQuestion ();
						point += multiplier * Configurations.BasePoint;
						txtPoint.text = string.Format(keyPoint, point);
					});
			} else {
				btnChoice.onClick.AddListener(
					() => {
						incorrect++;
						txtMarks.text = string.Format(keyMarks, correct, incorrect);
						LoadNextQuestion ();
					});
			}
		}

	}

	Button GetButton (Transform child) {
		return child.parent.GetComponent<Button> ();
	}

	public void SetQuestion (string input) {
		txtQuestion.text = input;
	}

	public void SetChoice (List<string> input) {
		for (int i = 0; i < input.Count; i++) {
			txtChoices[i].text = input[i];
		}
	}
}