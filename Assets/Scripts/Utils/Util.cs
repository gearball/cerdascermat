using System;
using System.Text.RegularExpressions;

namespace CerdasCermat.Utils {

	public class Util {

		public static string SuperTrim (string input) {
			return RemoveSpaces(input.Trim ().ToLower ());
		}

		static string RemoveSpaces (string input) {
			return Regex.Replace (input, @"\s+", "");
		}

		public static bool IsNumber(object value)
		{
			return 
				value is sbyte
					|| value is byte
					|| value is short
					|| value is ushort
					|| value is int
					|| value is uint
					|| value is long
					|| value is ulong
					|| value is float
					|| value is double
					|| value is decimal;
		}
	}
}

