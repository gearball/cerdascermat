﻿namespace SmartLocalization.Editor
{
	using UnityEngine;
	using UnityEngine.UI;
	using System.Collections;
	using TMPro;
	
	[RequireComponent(typeof(TextMeshProUGUI))]
	public class LocalizedTMPText  : MonoBehaviour
	{
		public string localizedKey = "INSERT_KEY_HERE";
		TextMeshProUGUI textObject;
		
		void Start() {
			textObject = this.GetComponent<TextMeshProUGUI>();
			
			//Subscribe to the change language event
			LanguageManager languageManager = LanguageManager.Instance;
			languageManager.OnChangeLanguage += OnChangeLanguage;

			//Run the method one first time
			OnChangeLanguage(languageManager);


		}
		
		void OnDestroy() {
			if (LanguageManager.HasInstance)
			{
				LanguageManager.Instance.OnChangeLanguage -= OnChangeLanguage;
			}
		}
		
		void OnChangeLanguage(LanguageManager languageManager) {
			textObject.text = LanguageManager.Instance.GetTextValue (localizedKey);
		}
	}
}