﻿namespace SmartLocalization.Editor
{
    using UnityEngine.UI;
    using UnityEngine;
    using UnityEditor;
    using System.Collections;

	[CustomEditor(typeof(LocalizedTMPText))]
    public class LocalizedTMPTextInspector : Editor
    {
        private string selectedKey = null;

        void Awake()
        {
			LocalizedTMPText textObject = ((LocalizedTMPText)target);
            if (textObject != null)
            {
                selectedKey = textObject.localizedKey;
            }
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            selectedKey = LocalizedKeySelector.SelectKeyGUI(selectedKey, true, LocalizedObjectType.STRING);

            if (!Application.isPlaying && GUILayout.Button("Use Key", GUILayout.Width(70)))
            {
				LocalizedTMPText textObject = ((LocalizedTMPText)target);
                textObject.localizedKey = selectedKey;
            }
        }

    }
}