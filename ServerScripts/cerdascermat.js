var Data = "Data";
var City = "City";
var School = "School";

var ScorePersonal = "ScorePersonal";
var ScoreNational = "ScoreNational";
var ScoreCity = "ScoreCity";

var StatisticsName = "StatisticsName";
var Value = "Value";


handlers.UpdateSchoolScore = function (args) {
	
	var data = server.GetTitleData ({});
	
	var obj;
	if (data.Data[Data] != null)
		obj = JSON.parse (data.Data[Data]);
	else
		obj = {};
	if (obj[City] == null) {
		obj[City] = {};
	}
	if (obj[City][args.City] == null) { 
		obj[City][args.City] = {};
		obj[City][args.City][School] = [];
	}
	if (obj[City][args.City][School].indexOf (args.School) == -1) { 
		obj[City][args.City][School].push (args.School);
	}
	
	var str = JSON.stringify(obj);
	
	server.SetTitleData ({
		Key: Data,
		Value: str
	});
	
	var schoolId = args.SchoolID;
	var score = args.Score;
	
	var statistic;
	var statistics = [];
	
	statistic = {};
	statistic[StatisticsName] = ScoreNational;
	statistic[Value] = score
	statistics.push (statistic);
	
	server.UpdatePlayerStatistics ({
		PlayFabId: schoolId,
		Statistics: 
	});
	
	return true;
}