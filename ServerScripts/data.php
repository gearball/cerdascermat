<?php

	$command = $_POST["Command"];
	
	$cmdPlainAdmin = "PlainAdmin";
	$cmdAddSchool = "AddSchool";
	$cmdAddUser = "AddUser";
	$cmdGetSchool = "GetSchool";
	
	$school = "School";
	$city = "City";
	$user = "User";
	
	$cmdPlainContent = "PlainContent";
	$cmdGetVersion = "GetVersion";
	$cmdGetCategory = "GetCategory";
	
	$version = "Version";
	$category = "Category";
	
	$cmdGetQuestion = "GetQuestion";
	
	$maxChoice = 4;
	$choice = "Choice";
	$question = "Question";
	$correct = "Correct";
	$incorrect = "Incorrect";
	
	$count = "Count";
	
	$fadmin = "admin.txt";
	$fcontent = "content.txt";
	$fversion = "version.txt";
	
	if (empty($command)) {
		echo "Nothing";
	} else {
		if (
		($command == $cmdPlainContent )
		) {
			$file = fopen($fcontent , "r") or die("Failed");
			$string = fread($file, filesize($fcontent));
			fclose($file);
			
			echo $string;
		} elseif (
		($command == $cmdPlainAdmin)
		) {
				
			$file = fopen($fadmin , "r") or die("Failed");
			$string = fread($file, filesize($fadmin));
			fclose($file);
			
			echo $string;
		} elseif (
		($command == $cmdAddSchool) || 
		($command == $cmdAddUser) ||
		($command == $cmdGetSchool)
		) {
			$file = fopen($fadmin , "r") or die("Failed");
			$string = fread($file, filesize($fadmin));
			fclose($file);
			
			$data = json_decode($string, true);
			
			if ($command == $cmdAddSchool) {
				$current = $data[$school][intval($_POST[$city])];
				if (!in_array($_POST[$school],$current)) {
					$data[$school][intval($_POST[$city])][] = $_POST[$school];
					
					$string = json_encode($data);
					
					$file = fopen($fadmin, "w+") or die("Unable to open file!");
					fwrite($file, $string);
					fclose($file);
				}
				
				echo "Success";
			} elseif ($command == $cmdAddUser) {
				if (!array_key_exists($_POST[$user], $data[$user])) {
					$schoolId = array_search($_POST[$school], $data[$school][intval($_POST[$city])]);
					$data[$user][$_POST[$user]] = array(intval($_POST[$city]), $schoolId);
					
					$string = json_encode($data);
				
					$file = fopen($fadmin, "w+") or die("Failed");
					fwrite($file, $string);
					fclose($file);
				}
				
				echo "Success";
			} elseif ($command == $cmdGetSchool) {
				echo json_encode($data[$school]);
			}
		} elseif (
		($command == $cmdGetCategory) ||
		($command == $cmdGetVersion)
		) {
			$filename = "";
			if ($command == $cmdGetCategory) {
				$filename = $fcontent;
			} elseif ($command == $cmdGetVersion) {
				$filename = $fversion;
			}
			$file = fopen($filename, "r") or die("Failed");
			$string = fread($file, filesize($fcontent));
			fclose($file);
			
			echo $string;
		} elseif (
		($command == $cmdGetQuestion) 
		) {
			$fquestion = "content-" . $_POST[$category] . ".txt";
			$file = fopen($fquestion , "r") or die("Failed");
			$string = fread($file, filesize($fquestion));
			fclose($file);
			
			$data = json_decode($string, true);
			
			$required = $_POST[$count];
			shuffle($data);
			
			$output = array();
			for ($i = 0; $i < $required; $i++) {
				shuffle($data[$i][$incorrect]);
				$choiceList = array();
				for ($j = 0; $j < $maxChoice - 1; $j++) {
					array_push($choiceList,$data[$i][$incorrect][$j]);
				}
				array_push($choiceList,$data[$i][$correct]);
				shuffle($choiceList);
				$correctIdx = array_search($data[$i][$correct],$choiceList);
				$set = array (
					$question => $data[$i][$question],
					$choice => $choiceList,
					$correct => $correctIdx
				);
				array_push($output,$set);
			}
			echo json_encode($output);
		}
	}
?>